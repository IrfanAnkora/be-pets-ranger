import { Request } from 'express';
import { UserSeed } from './user.interface';

export interface RequestCustom extends Request {
  user: UserSeed;
}
