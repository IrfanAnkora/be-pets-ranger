import { ObjectId } from 'mongodb';
export interface IUser {
  username: string;
  email: string;
  password: string;
  createdAt: string;
  updatedAt: string;
}

export interface UserCreate {
  username: string;
  email: string;
  password: string;
}

export interface UserSeed {
  _id: ObjectId;
  username: string;
  email: string;
  password: string;
  createdAt: string;
  updatedAt: string;
}
