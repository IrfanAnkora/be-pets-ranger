import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import { CONFIG } from './core/config';
import router from './routes';

const initialize = async () => {
  try {
    const { MONGO_URL, PORT } = process.env;
    if (!MONGO_URL) throw Error(`MONGO_URL required, received ${MONGO_URL}`);
    if (!PORT) throw Error(`PORT required, received ${PORT}`);

    const port = CONFIG.PORT || 4000;

    // Connect to Mongo database
    const db = await mongoose.connect(CONFIG.DB_URL || '');
    if (db) {
      console.info(`🟢 Successfully connected to Mongo database.`);
    } else {
      console.error(`🛑 Problem connecting to Mongo database.`);
    }

    const app = express();
    // Set body-parser to parse requests to JSON format
    app.use(bodyParser.json());
    // Set prefix for all routes and let app use router
    app.use('/api/v1', router);

    if (process.env.NODE_ENV !== 'test') {
      app.listen(port);
      console.info(`🚀 App running at port ${port}.`);
    }
    return app;
  } catch (error) {
    console.error(`❌ Problem with running server: ${error}`);
  }
};

export default initialize();
