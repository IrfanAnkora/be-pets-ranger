import bcrypt from 'bcrypt';
import User from '../models/user.model';
import { IUser, UserCreate, UserSeed } from '../interfaces/user.interface';
import { throwError } from '../core/errorResponse';
import { signJwt } from '../core/auth.middleware';
import { hideSensitiveData } from '../functions/user.functions';

const createUser = async (data: UserCreate): Promise<IUser> => {
  const { username, email, password } = data;
  const userEmail = email.toLocaleLowerCase();
  const SALT_ROUNDS = 10;
  const userExists = await User.findOne({
    $or: [
      {
        username,
      },
      {
        email: userEmail,
      },
    ],
  });
  if (userExists) {
    return throwError(
      'User already exists',
      { username, email: userEmail },
      409
    );
  }

  const hash = bcrypt.hashSync(password, SALT_ROUNDS);
  const user: UserSeed = await User.create({
    username,
    email: userEmail,
    password: hash,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  });

  return hideSensitiveData(user);
};

const getOneUser = async (userId: string) => {
  const user: UserSeed = await User.findById(userId);

  return hideSensitiveData(user);
};

const updateUser = async (userId: string, data: any) => {
  return await User.findOneAndUpdate(
    { _id: userId },
    { ...data, updatedAt: Date.now() }
  );
};

const deleteUser = async (userId: string) => {
  return await User.deleteOne({ _id: userId });
};

const login = async (data: any) => {
  const { email, password } = data;
  const userEmail = email.toLocaleLowerCase();

  const user = await User.findOne({ email: userEmail });
  if (!user) {
    return throwError('UserNotExist', { message: "User doesn't exist" }, 404);
  }

  const comparedPassword = bcrypt.compareSync(password, user.password);
  if (!comparedPassword) {
    return throwError('LoginFailed', { message: 'Wrong credentials' }, 401);
  }
  const bearerToken = await signJwt(user);

  return {
    bearerToken,
  };
};

const getMe = async (userId: string) => {
  return await User.findById(userId);
};

export const userService = {
  createUser,
  getOneUser,
  updateUser,
  deleteUser,
  login,
  getMe,
};
