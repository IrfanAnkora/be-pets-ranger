import { Router, Request, Response } from 'express';
import userRoutes from './user.routes';

const router = Router();

router.get('/test', (req: Request, res: Response) => {
  try {
    res.send({ message: `🟢 TEST: Application is working.` });
  } catch (e) {
    console.error(`🛑 TEST: Server error testing application.`, e);
    res.status(500).send(e);
  }
});

router.use('/users', userRoutes);

export default router;
