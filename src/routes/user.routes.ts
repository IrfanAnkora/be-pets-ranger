import { Router } from 'express';
import { userController } from '../controllers/user.controller';
import { authenticateJWT } from '../core/auth.middleware';

const userRoutes = Router();

userRoutes.route('/me').get(authenticateJWT, userController.getMe);

userRoutes.route('/').post(userController.createUser);

userRoutes.route('/:id').get(userController.getOneUser);

userRoutes.route('/:id').patch(userController.updateUser);

userRoutes.route('/:id').delete(userController.deleteUser);

userRoutes.route('/login').post(userController.login);

export default userRoutes;
