import { UserSeed } from '../interfaces/user.interface';

export const hideSensitiveData = (user: any) => {
  const userData: any = {};
  Object.keys(user.toJSON()).forEach(key => {
    if (key != 'password') {
      userData[key] = user[key];
    }
  });

  return userData;
};
