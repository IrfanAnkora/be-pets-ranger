import _ from 'lodash';
import Ajv from 'ajv';
import { UserCreate } from '../interfaces/user.interface';
import { throwError } from '../core/errorResponse';

const ajv = new Ajv();

const userCreate = async (data: UserCreate) => {
  const objectForCreate = _.pick(data, ['username', 'password', 'email']);
  const schema = {
    type: 'object',
    properties: {
      username: { type: 'string' },
      email: { type: 'string' },
      password: { type: 'string' },
    },
    required: ['username', 'email', 'password'],
    additionalProperties: true,
  };

  const validate = ajv.compile(schema);
  const valid = validate(objectForCreate);

  if (!valid) {
    const error = validate.errors[0];
    return throwError(error.message, error.params, 400);
  }
};

const userValidation = {
  userCreate,
};

export default userValidation;
