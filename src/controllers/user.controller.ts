import { Request, Response } from 'express';
import { generateFailure } from '../core/errorResponse';
import { UserCreate } from '../interfaces/user.interface';
import { userService } from '../services/user.service';
import userValidation from '../validations/user.validation';
import { RequestCustom } from '../interfaces/request.interface';

const createUser = async (req: Request, res: Response) => {
  try {
    const data: UserCreate = req.body;

    await userValidation.userCreate(data);
    const createdUser = await userService.createUser(data);

    return res.send(createdUser);
  } catch (e) {
    console.log(`user.controller in createUser method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

const getOneUser = async (req: Request, res: Response) => {
  try {
    const userId: string = req.params.id;
    const user = await userService.getOneUser(userId);

    return res.send(user);
  } catch (e) {
    console.log(`user.controller in getOneUser method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

const updateUser = async (req: Request, res: Response) => {
  try {
    const userId: string = req.params.id;
    const data = req.body;
    const user = await userService.updateUser(userId, data);

    return res.send(user);
  } catch (e) {
    console.log(`user.controller in updateUser method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

const deleteUser = async (req: Request, res: Response) => {
  try {
    const userId: string = req.params.id;
    const user = await userService.deleteUser(userId);

    return res.send(user);
  } catch (e) {
    console.log(`user.controller in deleteUser method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

const login = async (req: Request, res: Response) => {
  try {
    const data = req.body;
    const user = await userService.login(data);

    return res.send(user);
  } catch (e) {
    console.log(`user.controller in login method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

const getMe = async (req: RequestCustom, res: Response) => {
  try {
    const userId: string = req.user._id.toString();
    const user = await userService.getMe(userId);

    return res.send(user);
  } catch (e) {
    console.log(`user.controller in getMe method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

export const userController = {
  createUser,
  getOneUser,
  updateUser,
  deleteUser,
  login,
  getMe,
};
