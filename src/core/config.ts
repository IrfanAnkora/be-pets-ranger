import dotenv from 'dotenv';

dotenv.config({
  path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env.local',
});

export const CONFIG = {
  PORT: process.env.PORT || 4000,
  DB_URL: process.env.MONGO_URL,
  JWT_SECRET: process.env.JWT_SECRET,
};
