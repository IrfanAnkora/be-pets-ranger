import { NextFunction, Response } from 'express';
import jwt, { JwtPayload } from 'jsonwebtoken';

import { UserSeed } from '../interfaces/user.interface';
import { CONFIG } from '../core/config';
import { RequestCustom } from '../interfaces/request.interface';
import { generateFailure, throwError } from './errorResponse';
import User from '../models/user.model';

export const signJwt = async (user: UserSeed) => {
  const { _id, email } = user;
  const token = jwt.sign({ userId: _id, email }, CONFIG.JWT_SECRET, {
    expiresIn: '24h',
  });
  return token;
};

export const authenticateJWT = async (
  req: RequestCustom,
  res: Response,
  next: NextFunction
) => {
  try {
    const { authorization } = req.headers;

    if (!authorization) {
      await throwError('Unauthorized', { message: 'No auth found' }, 401);
    }

    if (!authorization.startsWith('Bearer ')) {
      await throwError('Unauthorized', { message: 'No bearer found' }, 401);
    }

    const token = authorization.split('Bearer ')[1];
    if (!token) {
      await throwError('Unauthorized', { message: 'No token found' }, 401);
    }

    let decoded: JwtPayload = null;
    try {
      decoded = jwt.verify(token, CONFIG.JWT_SECRET) as JwtPayload;
    } catch (err) {
      await throwError('Unauthorized', { message: 'Invalid token' }, 401);
    }

    const user = await User.findById(decoded.userId);
    if (!user) {
      await throwError('UserNotExists', { userId: decoded.userId }, 401);
    }

    req.user = user.toJSON();
    next();
  } catch (e) {
    console.log(`auth.middleware in authenticateJWT method error: ${e.name}`);
    return generateFailure(res, e);
  }
};
