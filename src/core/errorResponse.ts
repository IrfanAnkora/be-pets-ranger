import { Response } from 'express';
import { ErrorType } from '../interfaces/error.interface';

export const generateFailure = (res: Response, error: Error | ErrorType) => {
  let { name, data, statusCode } = error as ErrorType;
  if (!statusCode) {
    statusCode = 500;
    name = 'InternalError';
    data = { message: (error as Error).message };
  }
  return res.status(statusCode).send({
    statusCode,
    name,
    data,
  });
};

export const throwError = async (
  name: string,
  data: any,
  statusCode: number = 400
) => {
  throw {
    name,
    data,
    statusCode,
  };
};
