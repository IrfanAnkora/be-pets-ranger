import { throwError } from '../core/errorResponse';
import { usersSeed } from './user.seed';

const seedHandler = async () => {
  try {
    await usersSeed();
    return { status: 'ok' };
  } catch (e) {
    console.log(`Problem with seeding ${JSON.stringify(e)}`);
    return throwError('Problem with seeding', e.name, 500);
  }
};

seedHandler();
