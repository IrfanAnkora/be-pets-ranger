import bcrypt from 'bcrypt';
import mongoose from 'mongoose';
import { ObjectId } from 'mongodb';

import { throwError } from '../core/errorResponse';
import User from '../models/user.model';
import { CONFIG } from '../core/config';
import { UserSeed } from '../interfaces/user.interface';

export const usersSeed = async (): Promise<UserSeed[]> => {
  try {
    await mongoose.connect(CONFIG.DB_URL || '');

    const users = [
      {
        _id: new ObjectId('6250bfa5dbc19661a5bf2da7'),
        username: 'TestAcc',
        email: 'test@gmail.com',
        password: 'Test1!',
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('6250c072f987f607360e737a'),
        username: 'Irfo',
        email: 'irfan.mehanovic1@gmail.com',
        password: 'Irfo1!',
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('6250c08a48b7336983b32f14'),
        username: 'Admin',
        email: 'admin@gmail.com',
        password: 'Admin1!',
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
    ];

    const saltRounds = 10;
    const userPromises: any[] = [];

    users.map((user: UserSeed) => {
      const hash = bcrypt.hashSync(user.password, saltRounds);
      userPromises.push(User.create({ ...user, password: hash }));
    });

    await Promise.all(userPromises);
    console.log(`✅ Seedinng users finished successfully.`);
    mongoose.connection.close();
  } catch (e) {
    console.log(`Couldn't seed users ${e}`);
    return throwError('SeedingUsersError', e, 500);
  }
};
