import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../src/server';

import User from '../src/models/user.model';

const userTests = async () => {
  const generateTestUser = (username: string) => ({
    username,
    email: `${username}@gmail.com`,
    password: 'Test123!',
  });

  const user1 = { _id: '', data: generateTestUser('user1') };
  const user2 = { _id: '', data: generateTestUser('user2') };
  const user3 = { _id: '', data: generateTestUser('user3') };

  chai.use(chaiHttp);

  let server: any;
  describe(`User Tests`, async () => {
    before(async () => {
      server = await app;
      await User.deleteMany();
    });

    [user1, user2, user3].map(user =>
      it(`Should create user: ${user.data.username}`, async () => {
        const response = await chai
          .request(server)
          .post('/api/v1/users')
          .send(user.data);

        chai.expect(response.status).to.equal(200);
        chai.expect(response.body.username).to.equal(user.data.username);
        chai.expect(response.body.password).not.exist;

        switch (response.body.username) {
          case user1.data.username:
            user1._id = response.body._id;
            break;
          case user2.data.username:
            user2._id = response.body._id;
            break;
          case user3.data.username:
            user3._id = response.body._id;
            break;
        }
      })
    );
  });
};

export default userTests;
